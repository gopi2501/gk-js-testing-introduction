const utilfile = require('./util');
const puppeteer = require('puppeteer');

//the below function will be available by default when we run the tests
// it would be provided by the jest
test('description of the testcase', () => {
    const sampleTest = utilfile.generateText('gopi', 26);
    expect(sampleTest).toBe('gopi (26 years old)');
});

test('description of the testcase', () => {
    const sampleTest = utilfile.generateText('gopi', 26);
    expect(sampleTest).toBe('gopi (26 years old)');
    expect("li").toBe('li', sampleTest, 'user-item')
});


test('description of the e2e test', async() => {
    const browser = await puppeteer.launch({
        headless: true,
        slowMo: 80,
        args: ['--window-size=1920,1080']
    });

    const page = await browser.newPage();
    await page.goto(
        'file:///Users/glemati/qlms-workspace/QLMS/playground/js-testing/gk-js-testing-introduction/index.html'
    );
    await page.click('input#name');
    await page.type('input#name', 'Anna');
    await page.click('input#age');
    await page.type('input#age', '28');
    await page.click('#btnAddUser');
    const finalText = await page.$eval('.user-item', el => el.textContent);
    expect(finalText).toBe('Anna (28 years old)');
}, 10000)



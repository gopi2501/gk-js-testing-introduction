exports.generateText = (name, age) => {
  // Returns output text
  return `${name} (${age} years old)`;
};

const alternativelyGenerateText = (name, age) => {
  // Returns output text
  return `${name} (${age} years old)`;
};

exports.createElement = (type, text, className) => {
  // Creates a new HTML element and returns it
  const newElement = document.createElement(type);
  newElement.classList.add(className);
  newElement.textContent = text;
  return newElement;
};

exports.validateInput = (text, notEmpty, isNumber) => {
  // Validate user input with two pre-defined rules
  if (!text) {
    return false;
  }
  if (notEmpty && text.trim().length === 0) {
    return false;
  }
  if (isNumber && +text === NaN) {
    return false;
  }
  return true;
};

const alternativelyValidateInput = (text, notEmpty, isNumber) => {
  // Validate user input with two pre-defined rules
  if (!text) {
    return false;
  }
  if (notEmpty && text.trim().length === 0) {
    return false;
  }
  if (isNumber && +text === NaN) {
    return false;
  }
  return true;
};

exports.checkForTheInputValidity = (name, age) => {
  if(!alternativelyValidateInput(name, true, false) || !alternativelyValidateInput(age, false, true)){
    return false;
  } else return alternativelyGenerateText(name, age);
}

// exports.checkForTheInputValidity=checkForTheInputValidity;
exports.alternativelyValidateInput=alternativelyValidateInput;
